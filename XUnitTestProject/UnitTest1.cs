using CreditCardService;
using Moq;
using System;
using System.Collections.Generic;
using Xunit;
using XUnitMoqSample.Controllers;

namespace XUnitTestProject
{
    public class UnitTest1
    {
        public Mock<ICardDetailsService> mock = new Mock<ICardDetailsService>();
        [Fact]
        public void Test1()
        {

        }
        [Theory]
        [InlineData("9999999999",true)]
        [InlineData("5555555555", false)]
        public void TestCreditEligibility(string phoneNumber,bool expected)
        {
            mock.Setup(p => p.CheckCreditCardEligiblity(phoneNumber)).Returns(expected);

            CardDetailsController crd = new CardDetailsController(mock.Object);
            var actual = crd.CheckCreditCardEligiblity(phoneNumber);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCreditdetails()
        {            
            var objCard = new CreditScoreHistory()
            {
                PhoneNumber = "9999999999",
                Name = "aaaa",
                CreditScore = 760
            };
            mock.Setup(p => p.GetCreditdetails("9999999999")).Returns(objCard);

            CardDetailsController crd = new CardDetailsController(mock.Object);
            var actual = crd.GetCreditdetails("9999999999");
            Assert.False(objCard.Equals(actual));
        }
        [Fact]
        public void GetCreditScoreHistoryTest()
        {
            List<CreditScoreHistory> lst = new List<CreditScoreHistory>(){
            new CreditScoreHistory(){ PhoneNumber = "9999999999",Name="aaaa",CreditScore=760},
            new CreditScoreHistory(){ PhoneNumber = "5555555555",Name="dddd",CreditScore=740},
            new CreditScoreHistory(){ PhoneNumber = "4444444444",Name="ffff",CreditScore=650},
            new CreditScoreHistory(){ PhoneNumber = "7777777777",Name="gggg",CreditScore=820}
            };
            mock.Setup(p => p.GetCreditScoreHistory()).Returns(lst);
            CardDetailsController crd = new CardDetailsController(mock.Object);
            var actual = crd.GetCreditScoreHistory();
            Assert.True(actual.Equals(lst));

        }
        [Fact]
        public void AddCreditScoreHistoryTest()
        {
            var historyObj = new CreditScoreHistory() { PhoneNumber = "6666666666", Name = "kkkk", CreditScore = 860 };
            mock.Setup(p => p.AddCreditScoreHistory(It.IsAny<CreditScoreHistory>())).Returns("kkkk");
            CardDetailsController crd = new CardDetailsController(mock.Object);
            var actual = crd.AddCreditScoreHistory(historyObj);
            Assert.Equal("kkkk", actual);

        }
    }
}
