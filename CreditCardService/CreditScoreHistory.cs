﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CreditCardService
{
    public class CreditScoreHistory
    {
        [Key]
        public string PhoneNumber { get; set; }
        public int CreditScore { get; set; }
        public string Name { get; set; }
    }
}
