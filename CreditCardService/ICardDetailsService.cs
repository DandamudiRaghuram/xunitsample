﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreditCardService
{
    public interface ICardDetailsService
    {
        bool CheckCreditCardEligiblity(string phoneNumber);
        CreditScoreHistory GetCreditdetails(string phoneNumber);
        IEnumerable<CreditScoreHistory> GetCreditScoreHistory();
        string AddCreditScoreHistory(CreditScoreHistory history);
        bool DeleteHistoryByNumber(string phoneNumber);
    }
}
