﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CreditCardService
{
    public class CardDetailsService: ICardDetailsService
    {
        private CreditDBContext _context;
        public CardDetailsService(CreditDBContext context)
        {

            _context = context;
            if (!_context.CreditScoreHistory.Any())
            {
                _context.CreditScoreHistory.AddRange(new CreditScoreHistory() { PhoneNumber = "9999999999", Name = "aaaa", CreditScore = 760 },
            new CreditScoreHistory() { PhoneNumber = "5555555555", Name = "dddd", CreditScore = 740 },
            new CreditScoreHistory() { PhoneNumber = "4444444444", Name = "ffff", CreditScore = 650 },
            new CreditScoreHistory() { PhoneNumber = "7777777777", Name = "gggg", CreditScore = 820 });
                _context.SaveChanges();
            }
        }

        List<CreditScoreHistory> lst = new List<CreditScoreHistory>(){
            new CreditScoreHistory(){ PhoneNumber = "9999999999",Name="aaaa",CreditScore=760},
            new CreditScoreHistory(){ PhoneNumber = "5555555555",Name="dddd",CreditScore=740},
            new CreditScoreHistory(){ PhoneNumber = "4444444444",Name="ffff",CreditScore=650},
            new CreditScoreHistory(){ PhoneNumber = "7777777777",Name="gggg",CreditScore=820}
        };
        public bool CheckCreditCardEligiblity(string phoneNumber)
        {
            return lst.Any(x => x.PhoneNumber == phoneNumber && x.CreditScore > 750);
        }
        public CreditScoreHistory GetCreditdetails(string phoneNumber)
        {
            return lst.Where(x => x.PhoneNumber == phoneNumber).SingleOrDefault();
        }

        public IEnumerable<CreditScoreHistory> GetCreditScoreHistory()
        {
            return _context.CreditScoreHistory;
        }

        public string AddCreditScoreHistory(CreditScoreHistory history)
        {
            _context.CreditScoreHistory.Add(history);
            _context.SaveChanges();
            var name = _context.CreditScoreHistory.FirstOrDefault(i => i.PhoneNumber == history.PhoneNumber).Name;
            return name;
        }
        public bool DeleteHistoryByNumber(string phoneNumber)
        {
            var history = _context.CreditScoreHistory.FirstOrDefault(i => i.PhoneNumber == phoneNumber);
            if (history == null)
                return false;

            _context.CreditScoreHistory.Remove(history);
            _context.SaveChanges();

            return true;
        }
    }
}
