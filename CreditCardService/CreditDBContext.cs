﻿using CreditCardService;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CreditCardService
{
    public class CreditDBContext:DbContext
    {
        public CreditDBContext(DbContextOptions<CreditDBContext> options)
            : base(options)
        {
        }

        public DbSet<CreditScoreHistory> CreditScoreHistory { get; set; }
    }
}
