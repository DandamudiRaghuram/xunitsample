﻿using CreditCardService;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace XUnitMoqSample.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class CardDetailsController : ControllerBase
    {
        private readonly ICardDetailsService _cardDetailsService;

        public CardDetailsController(ICardDetailsService cardDetailsService)
        {
            _cardDetailsService = cardDetailsService;
        }

        [HttpGet]
        [Route("api/[controller]/CheckCreditCardEligiblity")]
        public bool CheckCreditCardEligiblity(string phoneNumber)
        {
            bool result = _cardDetailsService.CheckCreditCardEligiblity(phoneNumber);
            return result;
        }
        [HttpGet]
        [Route("api/[controller]/GetCreditdetails")]
        public CreditScoreHistory GetCreditdetails(string phoneNumber)
        {
            var result = _cardDetailsService.GetCreditdetails(phoneNumber);
            return result;
        }
        [HttpGet()]
        [Route("api/[controller]/GetCreditScoreHistory")]
        public IEnumerable<CreditScoreHistory> GetCreditScoreHistory()
        {
            return _cardDetailsService.GetCreditScoreHistory();
        }
        [HttpPost()]
        [Route("api/[controller]/AddCreditScoreHistory")]
        public string AddCreditScoreHistory(CreditScoreHistory history)
        {
            return _cardDetailsService.AddCreditScoreHistory(history);
        }
        [HttpDelete()]
        [Route("api/[controller]/DeleteHistoryByNumber")]
        public bool DeleteHistoryByNumber(string phoneNumber)
        {
            return _cardDetailsService.DeleteHistoryByNumber(phoneNumber);
        }
    }
}
